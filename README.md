# [`nix-statix`](https://git.dotya.ml/wanderer-containers/nix-statix)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/wanderer-containers/nix-statix/status.svg)](https://drone.dotya.ml/wanderer-containers/nix-statix)

the Containerfile in this repo simply installs `statix` on top of the base
image, the rationale being this image can be cached for reuse in CI (such as
[Drone](https://drone.io) with ephemeral containers).  
based on `docker.io/nixos/nix` (see tag for the `nix` version bundled), latest
version weekly rebuilt on cron.

### LICENSE
GPL-3.0-or-later (see [LICENSE](LICENSE) for details).
